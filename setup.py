from distutils.core import setup
import sys, os

version = '1.0'

setup(name='pscoil',
      version=version,
      description="Predictor of Coiled-coils based on evolutionary ans sequnce information",
      long_description="""\
Long description here ...""",
      classifiers=[], # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
      keywords='Bio-Informatics',
      author='Piero Fariselli',
      author_email='piero@biocomp.unibo.it',
      url='http://www.biocomp.unibo.it/piero',
      license='GPLv2',
      scripts=['pscoils/psCoils.py']
      )

#!/usr/bin/env python 
'''
    Copyright (C) 2007 Piero Fariselli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    psCoils.py  Copyright (C) 2007  Piero Fariselli
    contacts: Piero Fariselli
              e-mail: piero@biocomp.unibo.it, piero.fariselli@unibo.it
              Dept. of Biology
              University of Bologna
              via Irnerio 42
              40126 Bologna
              Italy
    This program comes with ABSOLUTELY NO WARRANTY; for details type psCoils.py.
    This is free software, and you are welcome to redistribute it
    under the condition of preserving this text 
'''

# Iports

import sys
import getopt
import math
import string

AAs="ACDEFGHIKLMNPQRSTVWY"
NewMat='''L 2.998 0.269 0.367 3.852 0.510 0.514 0.562
I 2.408 0.261 0.345 0.931 0.402 0.440 0.289
V 1.525 0.479 0.350 0.887 0.286 0.350 0.362
M 2.161 0.605 0.442 1.441 0.607 0.457 0.570
F 0.490 0.075 0.391 0.639 0.125 0.081 0.038
Y 1.319 0.064 0.081 1.526 0.204 0.118 0.096
G 0.084 0.215 0.432 0.111 0.153 0.367 0.125
A 1.283 1.364 1.077 2.219 0.490 1.265 0.903
K 1.233 2.194 1.817 0.611 2.095 1.686 2.027
R 1.014 1.476 1.771 0.114 1.667 2.006 1.844
H 0.590 0.646 0.584 0.842 0.307 0.611 0.396
E 0.281 3.351 2.998 0.789 4.868 2.735 3.812
D 0.068 2.103 1.646 0.182 0.664 1.581 1.401
Q 0.311 2.290 2.330 0.811 2.596 2.155 2.585
N 1.231 1.683 2.157 0.197 1.653 2.430 2.065
S 0.332 0.753 0.930 0.424 0.734 0.801 0.518
T 0.197 0.543 0.647 0.680 0.905 0.643 0.808
C 0.918 0.002 0.385 0.440 0.138 0.432 0.079
W 0.066 0.064 0.065 0.747 0.006 0.115 0.014
P 0.004 0.108 0.018 0.006 0.010 0.004 0.007'''
Parameters='''w  14 1.89 0.30 1.04 0.27 20
w  21 1.79 0.24 0.92 0.22 25
w  28 1.74 0.20 0.86 0.18 30
uw 14 1.82 0.28 0.95 0.26 20
uw 21 1.74 0.23 0.86 0.21 25
uw 28 1.69 0.18 0.80 0.18 30''' 

class Params:
     ''' define the Coils parameters '''
     def __init__(self,weighted='w',window=21):
         ''' initialize the parameters '''
         self.pow=[1.0]*7
         if weighted.lower() not in ['w','uw']:
             self.weight='uw'
         else:
             self.weight=weighted.lower()
         if self.weight=='w':
             self.pow[0]=self.pow[3]=2.5
         if window not in [14,21,28]:
             self.win=21
         else:
             self.win=window
         self.m_cc,self.sd_cc,self.m_g,self.sd_g,self.sc,self.mat=_initData(self.win,self.weight)


def _initData(window,weighted):
    ''' _initData builds the parameters starting from string '''
    from string import atof, atoi
    s=NewMat
    sl=s.split('\n')
    d={}
    for e in sl:
        v=e.split()
        for k in range(len(v[1:])):
            d[(v[0],k)]=atof(v[1:][k])
    s=Parameters
    sl=s.split('\n')
    for e in sl:
        v=e.split()
	if (v[0] == weighted) and (atoi(v[1])==window):
           vec=[]
           for n in v[2:]:
               vec.append(atof(n))        
    return vec+[d]
   


#
#
#---------------------------------#
def readProf(fname,aaOrder='VLIMFWYGAPSTCHRKQEND'):
    ''' readProf(fname,aaOrder) returns a dictionary profile prof[(pos,aa)]'''
    try:
        lines=open(fname,'r').readlines()
    except:
        sys.stderr.write('cannot open file '+fname+'\n')
        sys.exit()
    prof={}
    i=0
    while lines[i].find("#")>=0 and i <len(lines):
        pos=lines[i].find("ALPHABET=")
        if pos>0:
           v=lines[i][pos+len("ALPHABET="):].split()
           aaOrder=''.join(v[:20])
           
        i+=1
    if i >= len(lines):
        sys.stderr.write('Error in '+fname+'\n')
        sys.exit()
    pos=0
    while i<len(lines):
        v=lines[i].split()
        for j in range(len(aaOrder)):
            prof[(pos,aaOrder[j])]=string.atof(v[j])/100.0    
 #               prof[(pos,aaOrder[j])]=string.atoi(v[j])
        pos+=1
        i+=1
    return prof,pos
    
#---------------------------------#


#----------- readFasta files 
def readFasta(fname):
    ''' readFasta(fname) returns a dictionary 
    '''
    try:
        lines=open(fname,'r').readlines()
    except:
        sys.stderr.write('cannot open file '+fname+'\n')
        sys.exit()
    seqs={}
    name=''
    seq=''
    for line in lines:
        if line[0]=='>': # assuming fasta file
           if name != '':
               seqs[name]=seq
               seq=''
           name=line[1:].strip()
        else:
           seq+=''.join(line.split())
    if name != '':
        seqs[name]=seq
    return seqs
#---------------------------------#

#---------------------------------#
def coilProb(cScore,params):
    ''' coilProb(curScore,params) returns coiled-coils probability dependin on params'''
    t1=1/params.sd_cc
    t2=(cScore-params.m_cc)/params.sd_cc
    t4=t2*t2
    Gcc=t1*math.exp(-0.5*t4)
    t1=1/params.sd_g
    t2=(cScore-params.m_g)/params.sd_g
    t4=t2*t2
    Gg=t1*math.exp(-0.5*t4)
    return  Gg,Gcc,Gcc/(params.sc*Gg + Gcc )
#---------------------------------#

#---------------------------------#
def seqScore(mat,seq,sPos,hept_pos):
    ''' seqScore(mat,aa,pos) comoputes the sequenc based score'''
    try:
        retVal=mat[(seq[sPos],hept_pos)]
    except: 
        retVal=1
    return retVal
#---------------------------------#


#---------------------------------#
def profScore(mat,prof,sPos,hept_pos):
    ''' profScore(mat,aa,pos) comoputes the sequenc based score'''
    sum=0.0
    for aa in AAs:
        prof[(sPos,aa)]
        mat[(aa,hept_pos)]
        sum+=prof[(sPos,aa)]*mat[(aa,hept_pos)]
    return sum
#---------------------------------#

    
#---------------------------------#
def pred_coil(seq,seqLen,params,fScore):
    ''' pred_coil(seq,seqLen,params,fScore) returns the coiled coil prediction of sequence seq'''
    hept_pos=['a','b','c','d','e','f','g']
    score=[0.0]*seqLen
    prob=[0.0]*seqLen
    gcc=[0.0]*seqLen
    gg=[0.0]*seqLen
    hept_seq=['x']*seqLen
    for i in range(seqLen-params.win+1):
        this_score=1.0
        actual_win=0.0
        for j in range(min(params.win,seqLen-i)):
            pos=j%7
            actual_win+=params.pow[pos]
            this_score*=math.pow( fScore(params.mat,seq,i+j,pos), params.pow[pos] )
        if actual_win > 0:
            this_score=math.pow(this_score,1/actual_win)
        else:
            this_score=0.0
        for j in range(min(params.win,seqLen-i)): 
            pos=j%7
            if this_score > score[i+j]:
                score[i+j]=this_score
                hept_seq[i+j]=hept_pos[pos]
    for i in range(seqLen):
        gg[i],gcc[i],prob[i]= coilProb(score[i],params)
    return gg,gcc,prob,hept_seq,score
  
    
#---------------------------------#
def pred_coil2(seq,prof,params,ws=0.5):
    ''' pred_coil2(seq,prof,params,ws=0.5) returns the coiled coil prediction of sequence seq'''
    hept_pos=['a','b','c','d','e','f','g']
    seqLen=len(seq)
    score=[0.0]*seqLen
    prob=[0.0]*seqLen
    gcc=[0.0]*seqLen
    gg=[0.0]*seqLen
    hept_seq=['x']*seqLen
    for i in range(seqLen-params.win+1):
        this_score=1.0
        actual_win=0.0
        for j in range(min(params.win,seqLen-i)):
            pos=j%7
            actual_win+=params.pow[pos]
            avScore=(1-ws)*profScore(params.mat,prof,i+j,pos)+ws*seqScore(params.mat,seq,i+j,pos)
            this_score*=math.pow( avScore , params.pow[pos] )
        if actual_win > 0:
            this_score=math.pow(this_score,1/actual_win)
        else:
            this_score=0.0
        for j in range(min(params.win,seqLen-i)): 
            pos=j%7
            if this_score > score[i+j]:
                score[i+j]=this_score
                hept_seq[i+j]=hept_pos[pos]
    for i in range(seqLen):
        gg[i],gcc[i],prob[i]= coilProb(score[i],params)
    return gg,gcc,prob,hept_seq,score
  
#---------------------------------#
#---------------------------------#
def printCoil(seq,seqLen,hept_seq,score,P,Gcc,Gg,labels='T'):
    ''' '''
    ccLab="C"
    loopLab="L"
    if labels !='T' :
        for i in range(seqLen):
            #print "%4d %c %c %7.3f %7.3f (%7.3f %7.3f)" % (i+1,seq[i],hept_seq[i],score[i],P[i],Gcc[i],Gg[i])
            print "%4d %c %c %7.3f %7.3f %7.3f %7.3f" % (i+1,seq[i],hept_seq[i],score[i],P[i],Gcc[i],Gg[i])
    else:
        print " Pos A Hep Score   Prob    Gcc     Gg    Pred (Loop=L Coiledcoil=C)"
        for i in range(seqLen):
            if P[i] > 0.5:
               clabel=ccLab
            else:
               clabel=loopLab
            print "%4d %c %c %7.3f %7.3f %7.3f %7.3f %s" % (i+1,seq[i],hept_seq[i],score[i],P[i],Gcc[i],Gg[i],clabel)
    return
#---------------------------------#
#---------------------------------#
def usage(prog):
    ''' '''
    print "USAGE: ",prog, "-f fasta  -p profile [options]"
    print "Options:"
    print "       -W 14/21/28 # one of the possible windows (default 21)"
    print "       -w w/uw  # weight or unweight default=uw"
    print "       -l T/F  # print prediction labels when set T (default) if P>0.5"
    print "       -L [0.0,1.0] lambda value. It will be used only if both -f and -p are set "
    return 
#---------------------------------#

if __name__=='__main__':
    try:
       opts, args = getopt.getopt(sys.argv[1:], "f:p:w:W:l:L:")
    except:
       usage(sys.argv[0])
       sys.exit(2)
    weight='un'
    labels='T'
    wlambda=0.5
    win=21
    modefasta=modeprofile=False
    if not opts:
        usage(sys.argv[0])
        sys.exit(2)
    for o,a in opts:
        if o == '-f':
            seqs=readFasta(a)
            modefasta=True
        elif o == '-p':
            prof,profLen=readProf(a)
            modeprofile=True
        elif o == '-L':
            wlambda=string.atof(a)
        elif o == '-l':
            labels=a
        elif o == '-w':
            weight=a
        elif o == '-W':
            try:
                win=string.atoi(a)
            except:
                usage(sys.argv[0])
                sys.exit(2)
        else:
           usage(sys.argv[0])
           sys.exit(2)
    params=Params(weight,win)
    if modeprofile and modefasta: 
        #use both 
        for name in seqs.keys(): # WARNING We assume only one sequence!!!
            gg,gcc,prob,hept_seq,score=pred_coil2(seqs[name],prof,params,wlambda) 
            printCoil(seqs[name],len(seqs[name]),hept_seq,score,prob,gcc,gg,labels)
    elif modefasta:
        for name in seqs.keys():
#            print "> ",name      
            gg,gcc,prob,hept_seq,score=pred_coil(seqs[name],len(seqs[name]),params,seqScore)
            printCoil(seqs[name],len(seqs[name]),hept_seq,score,prob,gcc,gg,labels)
    elif modeprofile:
        gg,gcc,prob,hept_seq,score=pred_coil(prof,profLen,params,profScore)
        printCoil('x'*profLen,profLen,hept_seq,score,prob,gcc,gg,labels)
